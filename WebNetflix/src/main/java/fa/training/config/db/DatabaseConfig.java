package fa.training.config.db;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fa.training.config.AES;

@Configuration
public class DatabaseConfig {

	@Value("${db.datasource.url}")
	private String urlDB;

	@Value("${db.datasource.driverClassName}")
	private String driverClassName;

	@Value("${db.datasource.username}")
	private String userName;

	@Value("${db.datasource.password}")
	private String password;

	private String secretKey = "Aa@123456";

	@Bean(name = "dataSource")
	DataSource dataSource() {
		HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setJdbcUrl(AES.decrypt(urlDB, secretKey));
		hikariConfig.setDriverClassName(driverClassName);
		hikariConfig.setUsername(AES.decrypt(userName, secretKey));
		hikariConfig.setPassword(AES.decrypt(password, secretKey));

		return new HikariDataSource(hikariConfig);
	}

	@Bean(name = "transactionManager")
	DataSourceTransactionManager dataSourceTransactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}

	@Bean(name = "sqlSessionFactory")
	SqlSessionFactory sqlSessionFactory(@Qualifier("dataSource") DataSource dataSource) throws Exception {

		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);

		sqlSessionFactoryBean.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath:/fa/training/mapper/sql/*.xml"));

		return sqlSessionFactoryBean.getObject();
	}

}
