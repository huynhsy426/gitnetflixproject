	package fa.training.config.db;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@EnableWebSecurity
public class SercurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		// Tạo ra user trong bộ nhớ
		// lưu ý, chỉ sử dụng cách này để minh họa
		// Còn thực tế chúng ta sẽ kiểm tra user trong csdl
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(User.withDefaultPasswordEncoder() // Sử dụng mã hóa password đơn giản
				.username("sy").password("123").roles("USER") // phân quyền là người dùng.
				.build());
		return manager;
	}
	
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests(requests -> requests.antMatchers("/css/**").permitAll() // Cho phép tất cả mọi người truy cập vào 2 địa chỉ này
                .anyRequest().authenticated()).formLogin(login -> login.loginPage("/login").permitAll() // Cho phép người dùng xác thực bằng form login
                .defaultSuccessUrl("/projectList")).logout(logout -> logout.invalidateHttpSession(true).clearAuthentication(true).permitAll()); // Tất cả đều được truy cập vào địa chỉ này
	
	}


}
