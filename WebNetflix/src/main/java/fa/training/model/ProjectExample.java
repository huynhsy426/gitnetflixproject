package fa.training.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ProjectExample {
	
	protected String orderByClause;
	protected boolean distinct;
	protected List<Criteria> oredCriteria;

	public ProjectExample() {
		oredCriteria = new ArrayList<>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		protected void addCriterionForJDBCDate(String condition, Date value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			addCriterion(condition, new java.sql.Date(value.getTime()), property);
		}

		protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
			if (values == null || values.size() == 0) {
				throw new RuntimeException("Value list for " + property + " cannot be null or empty");
			}
			List<java.sql.Date> dateList = new ArrayList<>();
			Iterator<Date> iter = values.iterator();
			while (iter.hasNext()) {
				dateList.add(new java.sql.Date(iter.next().getTime()));
			}
			addCriterion(condition, dateList, property);
		}

		protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
		}

		public Criteria andProjectIdIsNull() {
			addCriterion("project_id is null");
			return (Criteria) this;
		}

		public Criteria andProjectIdIsNotNull() {
			addCriterion("project_id is not null");
			return (Criteria) this;
		}

		public Criteria andProjectIdEqualTo(Integer value) {
			addCriterion("project_id =", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotEqualTo(Integer value) {
			addCriterion("project_id <>", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdGreaterThan(Integer value) {
			addCriterion("project_id >", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("project_id >=", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdLessThan(Integer value) {
			addCriterion("project_id <", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdLessThanOrEqualTo(Integer value) {
			addCriterion("project_id <=", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdIn(List<Integer> values) {
			addCriterion("project_id in", values, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotIn(List<Integer> values) {
			addCriterion("project_id not in", values, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdBetween(Integer value1, Integer value2) {
			addCriterion("project_id between", value1, value2, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotBetween(Integer value1, Integer value2) {
			addCriterion("project_id not between", value1, value2, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectNameIsNull() {
			addCriterion("project_name is null");
			return (Criteria) this;
		}

		public Criteria andProjectNameIsNotNull() {
			addCriterion("project_name is not null");
			return (Criteria) this;
		}

		public Criteria andProjectNameEqualTo(String value) {
			addCriterion("project_name =", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameNotEqualTo(String value) {
			addCriterion("project_name <>", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameGreaterThan(String value) {
			addCriterion("project_name >", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameGreaterThanOrEqualTo(String value) {
			addCriterion("project_name >=", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameLessThan(String value) {
			addCriterion("project_name <", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameLessThanOrEqualTo(String value) {
			addCriterion("project_name <=", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameLike(String value) {
			addCriterion("project_name like", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameNotLike(String value) {
			addCriterion("project_name not like", value, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameIn(List<String> values) {
			addCriterion("project_name in", values, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameNotIn(List<String> values) {
			addCriterion("project_name not in", values, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameBetween(String value1, String value2) {
			addCriterion("project_name between", value1, value2, "projectName");
			return (Criteria) this;
		}

		public Criteria andProjectNameNotBetween(String value1, String value2) {
			addCriterion("project_name not between", value1, value2, "projectName");
			return (Criteria) this;
		}

		public Criteria andDeptIdIsNull() {
			addCriterion("dept_id is null");
			return (Criteria) this;
		}

		public Criteria andDeptIdIsNotNull() {
			addCriterion("dept_id is not null");
			return (Criteria) this;
		}

		public Criteria andDeptIdEqualTo(Integer value) {
			addCriterion("dept_id =", value, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdNotEqualTo(Integer value) {
			addCriterion("dept_id <>", value, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdGreaterThan(Integer value) {
			addCriterion("dept_id >", value, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("dept_id >=", value, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdLessThan(Integer value) {
			addCriterion("dept_id <", value, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdLessThanOrEqualTo(Integer value) {
			addCriterion("dept_id <=", value, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdIn(List<Integer> values) {
			addCriterion("dept_id in", values, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdNotIn(List<Integer> values) {
			addCriterion("dept_id not in", values, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdBetween(Integer value1, Integer value2) {
			addCriterion("dept_id between", value1, value2, "deptId");
			return (Criteria) this;
		}

		public Criteria andDeptIdNotBetween(Integer value1, Integer value2) {
			addCriterion("dept_id not between", value1, value2, "deptId");
			return (Criteria) this;
		}

		public Criteria andDifficultyIsNull() {
			addCriterion("difficulty is null");
			return (Criteria) this;
		}

		public Criteria andDifficultyIsNotNull() {
			addCriterion("difficulty is not null");
			return (Criteria) this;
		}

		public Criteria andDifficultyEqualTo(String value) {
			addCriterion("difficulty =", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyNotEqualTo(String value) {
			addCriterion("difficulty <>", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyGreaterThan(String value) {
			addCriterion("difficulty >", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyGreaterThanOrEqualTo(String value) {
			addCriterion("difficulty >=", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyLessThan(String value) {
			addCriterion("difficulty <", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyLessThanOrEqualTo(String value) {
			addCriterion("difficulty <=", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyLike(String value) {
			addCriterion("difficulty like", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyNotLike(String value) {
			addCriterion("difficulty not like", value, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyIn(List<String> values) {
			addCriterion("difficulty in", values, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyNotIn(List<String> values) {
			addCriterion("difficulty not in", values, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyBetween(String value1, String value2) {
			addCriterion("difficulty between", value1, value2, "difficulty");
			return (Criteria) this;
		}

		public Criteria andDifficultyNotBetween(String value1, String value2) {
			addCriterion("difficulty not between", value1, value2, "difficulty");
			return (Criteria) this;
		}

		public Criteria andInsTmIsNull() {
			addCriterion("ins_tm is null");
			return (Criteria) this;
		}

		public Criteria andInsTmIsNotNull() {
			addCriterion("ins_tm is not null");
			return (Criteria) this;
		}

		public Criteria andInsTmEqualTo(Date value) {
			addCriterionForJDBCDate("ins_tm =", value, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmNotEqualTo(Date value) {
			addCriterionForJDBCDate("ins_tm <>", value, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmGreaterThan(Date value) {
			addCriterionForJDBCDate("ins_tm >", value, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmGreaterThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("ins_tm >=", value, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmLessThan(Date value) {
			addCriterionForJDBCDate("ins_tm <", value, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmLessThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("ins_tm <=", value, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmIn(List<Date> values) {
			addCriterionForJDBCDate("ins_tm in", values, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmNotIn(List<Date> values) {
			addCriterionForJDBCDate("ins_tm not in", values, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("ins_tm between", value1, value2, "insTm");
			return (Criteria) this;
		}

		public Criteria andInsTmNotBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("ins_tm not between", value1, value2, "insTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmIsNull() {
			addCriterion("upd_tm is null");
			return (Criteria) this;
		}

		public Criteria andUpdTmIsNotNull() {
			addCriterion("upd_tm is not null");
			return (Criteria) this;
		}

		public Criteria andUpdTmEqualTo(Date value) {
			addCriterionForJDBCDate("upd_tm =", value, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmNotEqualTo(Date value) {
			addCriterionForJDBCDate("upd_tm <>", value, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmGreaterThan(Date value) {
			addCriterionForJDBCDate("upd_tm >", value, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmGreaterThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("upd_tm >=", value, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmLessThan(Date value) {
			addCriterionForJDBCDate("upd_tm <", value, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmLessThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("upd_tm <=", value, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmIn(List<Date> values) {
			addCriterionForJDBCDate("upd_tm in", values, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmNotIn(List<Date> values) {
			addCriterionForJDBCDate("upd_tm not in", values, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("upd_tm between", value1, value2, "updTm");
			return (Criteria) this;
		}

		public Criteria andUpdTmNotBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("upd_tm not between", value1, value2, "updTm");
			return (Criteria) this;
		}

		public Criteria andVersionIsNull() {
			addCriterion("version is null");
			return (Criteria) this;
		}

		public Criteria andVersionIsNotNull() {
			addCriterion("version is not null");
			return (Criteria) this;
		}

		public Criteria andVersionEqualTo(Integer value) {
			addCriterion("version =", value, "version");
			return (Criteria) this;
		}

		public Criteria andVersionNotEqualTo(Integer value) {
			addCriterion("version <>", value, "version");
			return (Criteria) this;
		}

		public Criteria andVersionGreaterThan(Integer value) {
			addCriterion("version >", value, "version");
			return (Criteria) this;
		}

		public Criteria andVersionGreaterThanOrEqualTo(Integer value) {
			addCriterion("version >=", value, "version");
			return (Criteria) this;
		}

		public Criteria andVersionLessThan(Integer value) {
			addCriterion("version <", value, "version");
			return (Criteria) this;
		}

		public Criteria andVersionLessThanOrEqualTo(Integer value) {
			addCriterion("version <=", value, "version");
			return (Criteria) this;
		}

		public Criteria andVersionIn(List<Integer> values) {
			addCriterion("version in", values, "version");
			return (Criteria) this;
		}

		public Criteria andVersionNotIn(List<Integer> values) {
			addCriterion("version not in", values, "version");
			return (Criteria) this;
		}

		public Criteria andVersionBetween(Integer value1, Integer value2) {
			addCriterion("version between", value1, value2, "version");
			return (Criteria) this;
		}

		public Criteria andVersionNotBetween(Integer value1, Integer value2) {
			addCriterion("version not between", value1, value2, "version");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table project
	 * @mbg.generated  Wed May 31 13:31:29 ICT 2023
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table project
     *
     * @mbg.generated do_not_delete_during_merge Wed May 31 13:30:41 ICT 2023
     */
    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }
}