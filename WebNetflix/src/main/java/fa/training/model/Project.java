package fa.training.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Project {
	
	private Integer projectId;
	private String projectName;
	private Integer deptId;
	private String difficulty;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date insTm;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updTm;
	private Integer version;
	
	

	public Project() {
		super();
	}

	
	public Project(Integer projectId, String projectName, Integer deptId, String difficulty, Date insTm, Date updTm,
			Integer version) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.deptId = deptId;
		this.difficulty = difficulty;
		this.insTm = insTm;
		this.updTm = updTm;
		this.version = version;
	}



	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public Date getInsTm() {
		return insTm;
	}

	public void setInsTm(Date insTm) {
		this.insTm = insTm;
	}

	public Date getUpdTm() {
		return updTm;
	}

	public void setUpdTm(Date updTm) {
		this.updTm = updTm;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", projectName=" + projectName + ", deptId=" + deptId
				+ ", difficulty=" + difficulty + ", insTm=" + insTm + ", updTm=" + updTm + ", version=" + version + "]";
	}
	
	public static String formatDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        return formatter.format(date);
    }
	
	public static void main(String[] args) {
        Date currentDate = new Date();
        System.err.println(currentDate);
        String formattedDate = formatDate(currentDate);
        System.out.println(formattedDate);
    }
	
	
}