package fa.training.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.training.mapper.DeptMapper;
import fa.training.model.Dept;
import fa.training.service.IDeptService;

@Service
public class DeptServiceImpl implements IDeptService{
	
	@Autowired
	DeptMapper deptMapper;

	@Override
	public List<Dept> selectDeptId() {
		return deptMapper.selectDeptId();
	}

	
}
