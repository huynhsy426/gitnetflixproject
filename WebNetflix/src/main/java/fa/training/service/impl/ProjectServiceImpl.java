package fa.training.service.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fa.training.mapper.ProjectMapper;
import fa.training.model.Project;
import fa.training.model.ProjectExample;
import fa.training.service.IProjectService;

@Service
public class ProjectServiceImpl implements IProjectService{

	@Autowired
	ProjectMapper projectMapper;

	@Override
	public Page<Project> findPaginated(Pageable pageable) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		ProjectExample projectExample = new ProjectExample();
		List<Project> projects = projectMapper.selectByExample(projectExample);
		List<Project> list;

		if (projects.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, projects.size());
			list = projects.subList(startItem, toIndex);
		}

		Page<Project> bookPage = new PageImpl<Project>(list, PageRequest.of(currentPage, pageSize), projects.size());

		return bookPage;
	}

	@Override
	public Page<Project> search(Pageable pageable, String inputSearch) {
		int pageSize = pageable.getPageSize();
		int currentPage = pageable.getPageNumber();
		int startItem = currentPage * pageSize;
		Project project = new Project();
		if (Pattern.matches("\\d+", inputSearch)) {
			project.setDeptId(Integer.valueOf(inputSearch));
			project.setProjectId(Integer.valueOf(inputSearch));
		}
		project.setProjectName(inputSearch);
		project.setDifficulty(inputSearch);

		List<Project> listProject = projectMapper.searchByInput(project);
		List<Project> list;

		if (listProject.size() < startItem) {
			list = Collections.emptyList();
		} else {
			int toIndex = Math.min(startItem + pageSize, listProject.size());
			list = listProject.subList(startItem, toIndex);
		}

		Page<Project> bookPage = new PageImpl<Project>(list, PageRequest.of(currentPage, pageSize), listProject.size());

		return bookPage;
	}
	
	@Override
	public void deleteById(Integer projectId) {
		projectMapper.deleteByPrimaryKey(projectId);
	}
	
	@Override
	public void insertProject(Project project) {
		project.setInsTm(new Date());
		project.setVersion(1);
		projectMapper.insert(project);
	}
	
	@Override
	public void updateProject(Project project) {
		project.setUpdTm(new Date());
		project.setVersion(project.getVersion() + 1);
		projectMapper.updateByPrimaryKey(project);
	}
	
	
}
