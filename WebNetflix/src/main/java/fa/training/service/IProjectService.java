package fa.training.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fa.training.model.Project;

public interface IProjectService {

	Page<Project> findPaginated(Pageable pageable);
	
	Page<Project> search(Pageable pageable, String inputSearch);
	
	void deleteById(Integer projectId);

	void insertProject(Project project);

	void updateProject(Project project);
	
	
}
