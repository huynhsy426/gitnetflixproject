package fa.training.service;

import java.util.List;

import fa.training.model.Dept;

public interface IDeptService {

	List<Dept> selectDeptId();
}
