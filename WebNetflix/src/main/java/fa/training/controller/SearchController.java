package fa.training.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fa.training.model.Project;
import fa.training.service.impl.ProjectServiceImpl;

@Controller
public class SearchController {

	@Autowired
	DataController dataController;

	@Autowired
	ProjectServiceImpl projectService ;
	
	
	
	
	/*
	 * Tạo một đối tượng để lưu tham số String chuyền vào 
	 * validate nó có phải số hay không nếu phải thì set vào Dept và ProjectId
	 * select dữ liệu theo đối tượng project
	 * Chuyển list project hiển thị lên trang
	 * */
	@GetMapping("/search")
	public ModelAndView searhByInput(@RequestParam("inputSearch") String inputSearch,
			@RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size) {
		dataController.setInputSerch(inputSearch);

		ModelAndView modelAndView = new ModelAndView("ProjectList");
		
		int currentPage = page.orElse(1);
		int pageSize = size.orElse(3);

		Page<Project> ProjectPage = projectService.search(PageRequest.of(currentPage - 1, pageSize),inputSearch);
		
		modelAndView.addObject("ProjectPage", ProjectPage);

		int totalPages = ProjectPage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
			modelAndView.addObject("pageNumbers", pageNumbers);
		}
		
		modelAndView.addObject("inputSearch", inputSearch);
		return modelAndView;
	}
	
}
