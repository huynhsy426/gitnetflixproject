package fa.training.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fa.training.mapper.ProjectMapper;
import fa.training.model.Project;
import fa.training.model.ProjectExample;
import fa.training.service.impl.DeptServiceImpl;
import fa.training.service.impl.ProjectServiceImpl;

@Controller
public class ProjectController {

	private static final String PROJECTLIST = "ProjectList";
	private static final String ADDORUPDATEPROJECT = "AddOrUpdateProject";
	private static final String UPDATE = "Update";
	private static final String BTNFUNCTION = "btnFunction";
	private static final String INPUTSEARCH = "inputSearch";
	private static final String PROJECTMODEL = "projectModel";
	private static final String SAVE = "Save";
	
	@Autowired
	DataController dataController;

	@Autowired
	ProjectMapper projectMapper;

	@Autowired
	DeptServiceImpl deptServiceImpl;

	@Autowired
	ProjectServiceImpl projectService;

	@GetMapping("/projectList")
	public ModelAndView listProject(@RequestParam(name = "feedbackDelete", required = false) Boolean feedbackDelete,
			@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {

		dataController.setInputSerch("");
		ModelAndView modelAndView = new ModelAndView(PROJECTLIST);
		pagination(modelAndView, page, size);
		
		if (feedbackDelete != null) {
			modelAndView.addObject(BTNFUNCTION, "Delete");

		}
		modelAndView.addObject(INPUTSEARCH, dataController.getInputSearch());

		return modelAndView;
	}

	@GetMapping("/addProject")
	public ModelAndView addProject() {

		ModelAndView modelAndView = new ModelAndView(ADDORUPDATEPROJECT);
		modelAndView.addObject(PROJECTMODEL, new Project());
		modelAndView.addObject(BTNFUNCTION, SAVE);
		modelAndView.addObject("lisDepts", deptServiceImpl.selectDeptId());

		return modelAndView;
	}

	@PostMapping("/addProject")
	public ModelAndView addProjectSuccess(@ModelAttribute(PROJECTMODEL) Project project,
			@RequestParam(BTNFUNCTION) String btnFunction, @ModelAttribute("deptId") String depString,
			@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {

		int count = 0;
		String nameProject = project.getProjectName();
		project.setDeptId(Integer.valueOf(depString));

		ModelAndView modelAndView = new ModelAndView(ADDORUPDATEPROJECT);

		ProjectExample exampleProjectId = new ProjectExample();
		ProjectExample exampleNameProject = new ProjectExample();

		// check project_Id
		exampleProjectId.createCriteria().andProjectIdEqualTo(project.getProjectId());
		List<Project> listProject = projectMapper.selectByExample(exampleProjectId);

		// check projectName
		exampleNameProject.createCriteria().andProjectNameEqualTo(project.getProjectName());
		List<Project> listprojectName = projectMapper.selectByExample(exampleNameProject);

		// Nếu là insert
		if (btnFunction.equals(SAVE)) {

			if (!listProject.isEmpty()) {
				modelAndView.addObject("IdExist", "Id da ton tai");
				count++;
			}

			if (projectNameIsExist(listprojectName)) {
				modelAndView.addObject("nameExist", "name da ton tai");
				count++;
			}

		}

		// Nếu là update
		if (btnFunction.equals(UPDATE) && equalName(project, nameProject, listprojectName)) {
			modelAndView.addObject("nameExist", "name da ton tai");
			count++;
		}
		// không có lỗi thì insert or update
		if (count == 0) {

			if (btnFunction.equals(SAVE)) {
				projectService.insertProject(project);
				modelAndView.addObject(BTNFUNCTION, SAVE);
				modelAndView.addObject("alertMessage", "Tạo mới thành công");
			} else {
				projectService.updateProject(project);
				modelAndView.addObject(BTNFUNCTION, UPDATE);
			}

			modelAndView = new ModelAndView(PROJECTLIST);
			modelAndView.addObject(INPUTSEARCH);

			pagination(modelAndView, page, size);

			return modelAndView;
		} else {
			modelAndView.addObject("lisDepts", deptServiceImpl.selectDeptId());
			modelAndView.addObject(PROJECTMODEL, project);
			modelAndView.addObject(BTNFUNCTION, SAVE);

		}

		return modelAndView;
	}

	@GetMapping("/updateProject")
	public ModelAndView updateOrCreate(@RequestParam("projectId") Integer projectId) {

		ModelAndView modelAndView = new ModelAndView(ADDORUPDATEPROJECT);
		modelAndView.addObject(BTNFUNCTION, UPDATE);
		Project project = projectMapper.selectByPrimaryKey(projectId);
		modelAndView.addObject(PROJECTMODEL, project);

		return modelAndView;
	}

	@GetMapping("/deleteProject")
	public String deleteById(@RequestParam("projectId") Integer projectId, @RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size) {

		projectService.deleteById(projectId);

		ModelAndView modelAndView = new ModelAndView(PROJECTLIST);

		pagination(modelAndView, page, size);

		modelAndView.addObject(INPUTSEARCH);
		return "redirect:/projectList?feedbackDelete=true";
	}

	public ModelAndView pagination(ModelAndView modelAndView, Optional<Integer> page, Optional<Integer> size) {
		int currentPage = page.orElse(1);
		int pageSize = size.orElse(5);
		Page<Project> projectPage;

		if (dataController.getInputSearch() == null) {
			projectPage = projectService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
		} else {
			projectPage = projectService.search(PageRequest.of(currentPage - 1, pageSize),
					dataController.getInputSearch());
		}

		modelAndView.addObject("ProjectPage", projectPage);
		 
		int totalPages = projectPage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
			modelAndView.addObject("pageNumbers", pageNumbers);
		}

		return modelAndView;
	}

	private static boolean projectNameIsExist(List<Project> listprojectName) {
		return !listprojectName.isEmpty();
	}

	private static boolean equalName(Project project, String nameString, List<Project> listprojectName) {
		return !project.getProjectName().equals(nameString) && projectNameIsExist(listprojectName);
	}

}
