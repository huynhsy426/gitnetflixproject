package fa.training.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import fa.training.mapper.UsersMapper;

@Controller
public class TestController {

	@Autowired
	UsersMapper usersMapper;

	/**
	 * @author SyTHH
	 * Index.
	 *
	 * @return the model and view
	 */
	@GetMapping("/login")
	public ModelAndView index() {

		ModelAndView modelAndView = new ModelAndView("login");

//		UsersExample example = new UsersExample();
//		example.createCriteria().andIdEqualTo(1).andUserNameEqualTo("a");
//		List<Users> listUsers = usersMapper.selectByExample(example);
//		for (Users value : listUsers) {
//			System.out.println("Kiem tra " + value.getFullName());
//		}
//		System.out.println("Kiem tra " + listUsers.size());
//		System.out.println(AES.encrypt("jdbc:sqlserver://localhost:1433;databaseName=DBNetFlix;encrypt=true;trustServerCertificate=true", "Aa@123456"));
//		System.out.println(AES.encrypt("sa", "Aa@123456"));
//		System.out.println(AES.encrypt("Sy0905198038", "Aa@123456"));

		Map<String, Object> param = new HashMap<>();
		param.put("userName", "a");
		param.put("id", "1");

		List<Map<String, Object>> listUsers = usersMapper.getAllUser(param);

		for (Map<String, Object> value : listUsers) {
			System.err.println("Kiem tra " + value.get("full_name"));
		}

		return modelAndView;
	}
}
