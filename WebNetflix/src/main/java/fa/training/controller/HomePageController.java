package fa.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import fa.training.mapper.UsersMapper;

@Controller
public class HomePageController {

	@Autowired
	UsersMapper usersMapper;

	@GetMapping(value = {"/home", "/"})
	public ModelAndView test() {
		ModelAndView modelAndView = new ModelAndView("home");
		return modelAndView;
	}
}
