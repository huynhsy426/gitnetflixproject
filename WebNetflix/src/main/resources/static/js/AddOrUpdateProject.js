$(document).ready(function() {
	var namePattern = /^[a-zA-Z0-9 ]+$/;
	var integerPattern = /^[0-9]+$/;


    function isNull(string) {
        if (string.trim() == '') {
          return false;
        } else {
          return true;
        }
      }
	function notValidate(selector, pattern) {
		return !pattern.test(selector.val());
	}
	var error;
	$('#register-btn').click(
		function(e) {
			error = 0;
			if (notValidate($('#projectId'), integerPattern) || !isNull($('#projectId').val())) {
				$('#projectId').addClass("is-invalid")
				
                if (notValidate($('#projectId'), integerPattern)) {
                    $('#projectId').next().text("Id khong dung")
                }
                if (!isNull($('#projectId').val())) {
                    $('#projectId').next().text("Khong duoc de trong")
                }
				error++;
			} else {
				$('#projectId').removeClass("is-invalid")
				$('#projectId').next().text("")
			}

			if (notValidate($('#projectName'), namePattern) || !isNull($('#projectName').val())) {
				$('#projectName').addClass("is-invalid")
				if (notValidate($('#projectName'), integerPattern)) {
                    $('#projectName').next().text("project Name khong dung")
                }
                if (!isNull($('#projectName').val())) {
                    $('#projectName').next().text("Khong duoc de trong")
                }
				error++;
			} else {
				$('#projectName').removeClass("is-invalid")
                $('#projectName').next().text("")
			}

            if ($('#deptId').val().trim() === '') {
				$('#deptId').addClass("is-invalid")
                if (!isNull($('#deptId').val())) {
                    $('#deptId').next().text("Khong duoc de trong")
                }
				error++;
			} else {
				$('#deptId').removeClass("is-invalid")
                $('#deptId').next().text("")
			}

            if (notValidate($('#difficulty'), integerPattern) || !isNull($('#difficulty').val())) {
				$('#difficulty').addClass("is-invalid")
				if (notValidate($('#difficulty'), integerPattern)) {
                    $('#difficulty').next().text("difficulty  khong dung")
                }
                if (!isNull($('#difficulty').val())) {
                    $('#difficulty').next().text("Khong duoc de trong")
                }
				error++;
			} else {
				$('#difficulty').removeClass("is-invalid")
                $('#difficulty').next().text("")
			}



			
			
			if (error != 0) {
				e.preventDefault();
			}else{
                e.preventDefault();
                Swal.fire({
                    title: 'are you sure you want to insert into DB?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: "Yes, I'm sure!"
                  }).then((result) => {
                    if (result.isConfirmed) {
                      $("form:last").trigger("submit")
                    }
                  })
            }
		}
	);
	
	
	


})


