	$(".hrefDelete").click(function(e) {
		e.preventDefault();
		var currentRow = $(this).closest("tr");
	    Swal.fire({
	        title: 'Are you sure?',
	        text: "You won't be able to revert this!",
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonColor: '#3085d6',
	        cancelButtonColor: '#d33',
	        confirmButtonText: 'Yes, delete it!',
	        cancelButtonText: 'Back'
	    }).then((result) => {
	        if (result.isConfirmed) {
	           window.location.href = this.href;
	        }
	    })
	})



