$(document).ready(function() {
	var namePattern = /^[a-zA-Z ]+$/;
	var sdtPattern = /^[0-9]{10}$/;
	var emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
	function isNull(value) {
		return value.trim() === null;
	}
	function notValidate(selector, pattern) {
		return !pattern.test(selector.val());
	}
	var error;
	$('#register-btn').click(
		function(e) {
			error = 0;
			if (notValidate($('#firstname'), namePattern) || isNull($('#firstname').val())) {
				$('#firstname').addClass("is-invalid")
				$('#firstname').next().text("Name khong dung")
				error++;
			} else {
				$('#firstname').removeClass("is-invalid")
			}
			if (notValidate($('#lastname'), namePattern) || isNull($('#lastname').val())) {
				$('#lastname').addClass("is-invalid")
				$('#lastname').next().text("LastName khong dung")
				error++;
			} else {
				$('#lastname').removeClass("is-invalid")
			}
			
			if($('#inlineFormCustomSelect').find(":selected").val() == "Option"){
				$('#inlineFormCustomSelect').addClass("is-invalid")
				$('#inlineFormCustomSelect').next().text("Chưa chọn")
				error++;
			} else {
				$('#inlineFormCustomSelect').removeClass("is-invalid")
			}
			
			
			if (notValidate($('#telephone'), sdtPattern)) {
				$('#telephone').addClass("is-invalid")
				$('#telephone').next().text("telephone khong dung")
				error++;
			} else {
				$('#telephone').removeClass("is-invalid")
			}
			if (notValidate($('#email'), emailPattern)) {
				$('#email').addClass("is-invalid")
				$('#email').next().text("email khong dung")
				error++;
			} else {
				$('#email').removeClass("is-invalid")
			}
			if ($('input[name="country"]:checked').val() === undefined) {
				$('input[name="country"]:checked').addClass("is-invalid")
				$('input[name="country"]:checked').next().text("Please select where you are in!!!");
				error++;
			}
			if (error != 0) {
				e.preventDefault();
			}
		}
	);
	$('#update-btn').click(
		function(e) {
			error = 0;
			if (notValidate($('#firstname'), namePattern) || isNull($('#firstname').val())) {
				alert("First name is not valid!");
				error++;
			}
			if (notValidate($('#lastname'), namePattern) || isNull($('#lastname').val())) {
				alert("Last name is not valid!");
				error++;
			}
			if (notValidate($('#telephone'), sdtPattern)) {
				alert("Phone number is not valid!");
				error++;
			}
			if (notValidate($('#email'), emailPattern)) {
				alert("Email is not valid!");
				error++;
			}
			if ($('input[name="country"]:checked').val() === undefined) {
				alert("Please select where you are in!!!");
				error++;
			}
			if (error != 0) {
				e.preventDefault();
			}
		}
	);

	$('#help-register-btn').click(function(e) {
		e.preventDefault();
		$.ajax({
			url: './view/help.jsp',
			type: 'GET',
			success: function(response) {
				$('#help-register').html(response);
			},
			error: function(xhr, status, error) {
				console.error('Ajax request error:', error);
			}
		});

	});


})


